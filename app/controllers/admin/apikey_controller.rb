class Admin::ApikeyController < ApplicationController
  before_action :authenticate_user!
  before_action :select_apkey, :only => [:edit, :update, :destroy]

  def index
    @apikeys = Apikey.all.order(:created_at => :asc)
    gon.apikeys = @apikeys.each{|k| k.secret = ''; k}
    # if !@newApiKey.nil?
      gon.newApiKey = @newApiKey
    # end
  end

  def create
    #SecureRandom.base64(32)
    apikey = Apikey.new
    apikey.key = (0..16).map { ('a'..'z').to_a[rand(26)] }.join
    secret = SecureRandom.base64(32)
    apikey.secret = Digest::SHA2.new(512).hexdigest(secret)
    begin
      apikey.save!
      flash[:success] = 'API Key generated!'
      flash[:new_token] = apikey.key
      flash[:new_secret] = secret
    rescue
      flash[:error] = 'Generate API key failed!'
    end

    redirect_to admin_apikey_index_path
  end

  def edit
  end

  def update
    @apikey.remarks = params.require(:apikey).permit(:remarks)[:remarks]
    begin
      @apikey.save!
      flash[:success] = 'Remarks saved!'
    rescue
      flash[:error] = 'Save failed!'
    end
    redirect_to admin_apikey_index_path
  end

  def destroy
    begin
      @apikey.destroy!
      flash[:success] = 'API Key deleted!'
    rescue
      flash[:error] = 'Delete failed!'
    end
    redirect_to :back
  end

  def select_apkey
    @apikey = Apikey.find(params.permit(:id)[:id].to_i)
  end
end
