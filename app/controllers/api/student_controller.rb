class Api::StudentController < ApplicationController
  before_action :api_auth

  def all
    @students = Student.all
    respond_to do |format|
      format.xml { render :xml => @students.to_xml(:only => [:id, :name, :gender]) }
      format.json { render :json => @students.to_json(:only => [:id, :name, :gender]) }
      # format.json do
      #   render :json => @students.to_json
      # end
    end
  end

  def one
    @student = []
    begin
      @student = Student.find(params['id'].to_i)
    rescue

    end
    respond_to do |format|
      format.xml { render :xml => @student.to_xml(:only => [:id, :name, :gender, :remarks]) }
      format.json { render :json => @student.to_json(:only => [:id, :name, :gender, :remarks]) }
    end
  end

  private

  def api_auth
    if params.has_key?(:token)
      #auth by apikey
      apikey = Apikey.find_by_key(params[:token])

      if apikey.nil?
        #key not found
        render json:{status:401, statusText:'unauthorized'}, :status => :unauthorized
        return false
      elsif apikey.secret == Digest::SHA2.new(512).hexdigest(params[:secret])
        #key match secret
        return true
      else
        #key not match secret
        render json:{status:401, statusText:'unauthorized'}, :status => :unauthorized
        return false
      end
    else
      #auth by login
      if signed_in?
        return true
      else
        render json:{status:401, statusText:'unauthorized'}, :status => :unauthorized
        return false
      end
    end
  end
end
