class Api::GameResultController < ApplicationController
  before_action :api_auth

  #create new game result record
  def new
    #limit params
    form_data = params.permit(:student_id, :score, :duration, :object_total, :object_caught, :scene_id)
    game_result = GameResult.new(form_data)

    #check student_id since sqlite foreign key seem not working
    student = nil
    begin
      student = Student.find(form_data[:student_id].to_i)
    rescue

    end

    game_result.id = GameResult.maximum(:id) + 1 # for postgresql

    #save to database
    if !student.nil? and game_result.save!
      render json:{status:'OK'}
    else
      render json:{status:'FAIL', data:game_result}
    end
  end

  #query game record by student id
  def history
    if params.has_key? :student_id
      id = params.permit(:student_id)[:student_id].to_i
      # results = GameResult.where(student_id: id)
      if params.has_key? :limit and params[:limit].to_i > 0
        results = GameResult.where(student_id:id).order(created_at: :desc).limit(params[:limit].to_i)
      else
        results = GameResult.where(student_id:id).order(created_at: :desc)
      end
      respond_to do |format|
        format.xml { render :xml => results.to_xml(:only => [:id, :score, :duration, :object_total, :object_caught, :scene_id, :created_at]) }
        format.json { render :json => results.to_json(:only => [:id, :score, :duration, :object_total, :object_caught, :scene_id, :created_at]) }
      end
    else
      results = GameResult.joins(:student).select('game_results.id, game_results.student_id, students.name, game_results.score,game_results.duration,game_results.object_total,game_results.object_caught,game_results.scene_id,game_results.created_at').order(created_at: :desc)
      respond_to do |format|
        format.xml { render :xml => results.to_xml(:only => [:id, :student_id,:name, :duration, :object_total, :object_caught, :scene_id, :created_at]) }
        format.json { render :json => results.to_json(:only => [:id, :student_id, :name, :duration, :object_total, :object_caught, :scene_id, :created_at]) }
      end
    end
  end

  def statistic
    id = params.permit(:id)[:id]
    case id
      when 'gender_ratio'
        m = Student.where(:gender => 0).count
        f = Student.where(:gender => 1).count
        mRatio = 0
        fRatio = 0
        if m+f > 0
          mRatio = m.to_d/(m+f)
          fRatio = f.to_d/(m+f)
          #may be add 'gender not available in db later'
        end
        render json: {
                   'M': {
                       'count': m,
                       'ratio': mRatio
                   },
                   'F': {
                       'count': f,
                       'ratio': fRatio
                   }
               }
      when 'record_freq'
        render json: GameResult.order('DATE(created_at)').group('DATE(created_at)').count
      when 'duration_avg'
        render json:{
                   'all':{
                       'all': GameResult.average(:duration),
                       'byDate': GameResult.group('DATE(created_at)').order('DATE(created_at)').average(:duration)
                   },
                   'M': {
                       'all': GameResult.joins(:student).where('students' => {'gender' => 0}).average(:duration),
                       'byDate': GameResult.joins(:student).where('students' => {'gender' => 0}).group('DATE(game_results.created_at)').order('DATE(game_results.created_at)').average(:duration)
                   },
                   'F': {
                       'all': GameResult.joins(:student).where('students' => {'gender' => 1}).average(:duration),
                       'byDate': GameResult.joins(:student).where('students' => {'gender' => 1}).group('DATE(game_results.created_at)').order('DATE(game_results.created_at)').average(:duration)
                   }
               }
      when 'duration_student_sum'
        studSum = GameResult.joins(:student).group('game_results.student_id').sum('game_results.duration').to_a
        studSum.collect { |entry|
          entry[2]= Student.find(entry[0]).name
          recentRecord = GameResult.where(:student_id => entry[0]).order(:created_at => :desc).limit(2)
          if(recentRecord.length > 1)
            recordDiff = recentRecord[0].duration - recentRecord[1].duration
            if(recordDiff.abs > 60)
              entry[3] = recordDiff
            else
              entry[3] = 0
            end
          else
            entry[3] = 0
          end

        }
        #[student_id, sum of duration, student name, duration diff (show when diff>60 sec)]
        render json: studSum
      when 'object_catch_able'
        can_catch = GameResult.group(:student_id).having('SUM("game_results"."object_caught") > 0').sum(:object_caught).count
        cannot_catch = GameResult.group(:student_id).having('SUM("game_results"."object_caught") = 0').sum(:object_caught).count
        render json:{
                   can: can_catch,
                   cannot: cannot_catch,
                   unknow: (Student.count - can_catch - cannot_catch)
               }
      when 'object_catch_avg'
        render json:{
                   all:GameResult.where('object_caught > 0').average(:object_caught),
                   byDate: GameResult.where('object_caught > 0').group('DATE(created_at)').order('DATE(created_at)').average(:object_caught)
               }
      when 'scene_playtimes'
        render json: {
                   scene1: GameResult.where('scene_id = 1').order('DATE(created_at)').group('DATE(created_at)').count.to_a,
                   scene2: GameResult.where('scene_id = 2').order('DATE(created_at)').group('DATE(created_at)').count.to_a
               }
      else
        render json:{status:400, statusText:'bad request'}, :status => :bad_request
    end

  end

  def strong_params(params)
    params.require(:game_result).permit(:student_id, :score, :duration, :object_total, :object_caught)
  end

  private

  def api_auth
    if params.has_key?(:token)
      #auth by apikey
      apikey = Apikey.find_by_key(params[:token])

      if apikey.nil?
        #key not found
        render json:{status:401, statusText:'unauthorized'}, :status => :unauthorized
        return false
      elsif apikey.secret == Digest::SHA2.new(512).hexdigest(params[:secret])
        #key match secret
        return true
      else
        #key not match secret
        render json:{status:401, statusText:'unauthorized'}, :status => :unauthorized
        return false
      end
    else
      #auth by login
      if signed_in?
        return true
      else
        render json:{status:401, statusText:'unauthorized'}, :status => :unauthorized
        return false
      end
    end
  end
end
