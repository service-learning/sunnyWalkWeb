class StudentsController < ApplicationController
  before_action :authenticate_user!
  before_action :select_student, only: [:show, :edit, :update, :destroy]
  include ApplicationHelper
  def index
  end

  def new
    @student = Student.new
  end

  def create
    form_data = strong_params params
    form_data[:gender] = form_data[:gender].to_i
    @student = Student.new form_data
    @student.id = Student.maximum(:id) + 1 # for postgresql
    if @student.save
      flash[:notice] = "Student added: #{@student.name}"
      redirect_to new_student_path
    else
      flash[:error] = 'Add student failed, read errors below.'
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    form_data = strong_params(params)
    old_name = @student.name
    if @student.update(form_data)
      flash[:success] = "Student \"#{@student.name}\" updated"
      redirect_to student_path(@student)
    else
      if form_data['name'] == ''
        @student.name = old_name
      end
      flash[:error] = 'Student update failed, read errors below.'
      render :edit
    end
  end

  def destroy
    if @student.destroy
      flash[:success] = "Student \"#{@student.name}\" deleted."
      redirect_to students_path
    else
      flash[:error] = 'Delete failed. May be some records are using it.'
      redirect_to student_path(@student)
    end
  end

  def select_student
    @student
    begin
      @student = Student.find(params[:id].to_i)
    rescue

    end
  end

  def strong_params(params)
    params.require(:student).permit(:name, :gender, :remarks)
  end

end
