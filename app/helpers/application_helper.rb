module ApplicationHelper
  def bs_nav_active_class(link_path)
    current_page?(link_path) ? 'active' : ""
  end

  def load_js(*files)
    content_for(:head) { javascript_include_tag(*files) }
  end

  def convert_gender(student_form)
    case student_form[:gender]
      when '0'
        student_form[:gender] = 'M'
      when '1'
        student_form[:gender] = 'F'
    end
    student_form
  end
end
