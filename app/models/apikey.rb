class Apikey < ActiveRecord::Base

  validates :key, :presence => true
  validates :secret, :presence => true
end
