class Student < ActiveRecord::Base
  has_many :game_results, foreign_key: :student_id

  enum gender: [:M, :F]

  validates :name, :presence => true
  validates :gender, :presence => true, :inclusion => {in:['M', 'F']}
end
