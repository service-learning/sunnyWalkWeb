class GameResult < ActiveRecord::Base
  belongs_to :student

  # validates :score, :presence => true,numericality:{greater_than_or_equal_to: 0}
  validates :duration, :presence => true,numericality:{greater_than_or_equal_to: 0}
  validates :object_total, :presence => true,numericality:{greater_than_or_equal_to: 0}
  validates :object_caught, :presence => true,numericality:{greater_than_or_equal_to: 0}
  validates :scene_id, :presence => true,numericality:{greater_than_or_equal_to: 1}
end
