$(->
  if gon.apikeys? and gon.apikeys isnt undefined and gon.apikeys.length > 0
    for v,k in gon.apikeys.reverse()
      $('#apikey-show-container').append(
        keyContainer = $('<div />').addClass('col-md-6').append(
          # data
          well = $('<div />').addClass('well').append(
            $('<div />').append('<span>Access Token: </span>').append($('<span />').text(v.key))
          )
  #        .append(
  #          $('<div />').append('<span>Secret: </span>').append($('<span />').addClass('api-secret').text(v.secret))
  #        )
          .append(
            $('<div />').append("<span>Created at: </span><span>#{moment(v.created_at).format('DD-MMM-YYYY, h:mm A')}</span>")
          ).append(

          )
          if v.remarks?
            well.append($('<div />').append('<span>Remarks: </span>').append($('<span />').text(v.remarks)))
          #buttons
          well.append(
            $('<a />').addClass('btn btn-xs btn-warning').attr({href:"/admin/apikey/#{v.id}/edit"}).append($('<span/>').addClass('glyphicon glyphicon-wrench'))
          ).append(' ').append(
            $('<a />').attr({
              href:"/admin/apikey/#{v.id}",
              'data-method':'delete',
              'data-confirm':"Delete key '#{v.key}'?"
            }).addClass('btn btn-xs btn-danger').append($('<span/>').addClass('glyphicon glyphicon-trash'))
          )
        )
      );
)