($ ->
  console.log 'game_result#index'
  $.getJSON "/api/game_result/history.json", (data)->
    dataSet = [];
    for k,v of data
      dataSet.push([
        1+parseInt(k),
        $("<a/>").text(v.name).attr({href:'/students/'+v.student_id})[0].outerHTML,
#        $("<a/>").text(v.score).attr({href:'/game_results/'+v.id})[0].outerHTML,
        v.duration + ' s',
        Scene[parseInt(v.scene_id)].name,
        v.object_total,
        v.object_caught,
        moment(v.created_at).format('DD-MMM-YYYY, h:mm A'),
      ])

    $("#table-of-records").DataTable {
      responsive:true,
      data:dataSet,
      columns:[
        {title:'#'},
        {title:'Student'},
#        {title:'Score'},
        {title:'Duration'},
        {title:'Scene'},
        {title:'Total Object'},
        {title:'Caught Object'},
        {title:'Date Time'}
      ],
      "bDestroy": true
    }
)