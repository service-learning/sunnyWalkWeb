($ ->
  console.log 'student#index'
  $.getJSON "/api/student/all.json", (data)->
    dataSet = [];
    for k,v of data
      dataSet.push([
        1+parseInt(k),
        $("<a/>").text(v.name).attr({href:'/students/'+v.id})[0].outerHTML,
        v.gender,
      ])

    $("#table-of-students").DataTable {
      responsive:true,
      data:dataSet,
      columns:[
        {title:'#'},
        {title:'Name'},
        {title:'Gender'}
      ],
      "bDestroy": true
    }
)