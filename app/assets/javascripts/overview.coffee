# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(->
  chartsDiv = [
    'ov-basic-gender',
    'ov-basic-record-freq',
    'ov-duration-avg',
    'ov-duration-avg-female',
    'ov-duration-avg-male',
    'ov-duration-student-total',
    'ov-scene-playtimes',
    'ov-object-able-to-catch',
    'ov-object-avg-catch'
  ]
  for v, k in chartsDiv
    $('#' + v).mdLoading()

  google.charts.setOnLoadCallback Charts.Overview.genderRatio.draw
  google.charts.setOnLoadCallback Charts.Overview.recordFreq.draw
  google.charts.setOnLoadCallback Charts.Overview.durationAvg.drawAll
  google.charts.setOnLoadCallback Charts.Overview.durationAvg.drawMale
  google.charts.setOnLoadCallback Charts.Overview.durationAvg.drawFemale
  google.charts.setOnLoadCallback Charts.Overview.durationAvg.drawStudentTotal
  google.charts.setOnLoadCallback Charts.Overview.scenePlaytimes.draw
  google.charts.setOnLoadCallback Charts.Overview.objectCatchAble.draw
  google.charts.setOnLoadCallback Charts.Overview.objectCatchAvg.draw
)