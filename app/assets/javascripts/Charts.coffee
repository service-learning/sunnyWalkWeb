@Charts = {
  Student:{
    show:{
      durationOverTime: ->
        $.getJSON "/api/game_result/history.json?student_id=#{student_info.id}", (json)->
          if json.length == 0
            $('#student-show-game-duration').html($('<div/>').addClass('text-center text-primary').text('No data.'))
          else
            charData = [['Datetime', 'Duration']]
            sumDurationSec = 0
            for data, index in json
              sumDurationSec += data.duration
              charData.push([new Date(data.created_at), data.duration/60])
            avgDurationSec = sumDurationSec/json.length
            charData = google.visualization.arrayToDataTable(charData);
            options = {
              chart: {
                title: 'Duration over time',
                subtitle: "#{student_info.name}, #{student_info.gender}. Average duration: #{parseInt(avgDurationSec/60)}min #{parseInt(avgDurationSec%60)}s"
              },
              legend: { position: 'bottom' },
              curveType: 'function',
              hAxis: {
                format: "dd/MMM HH:mm"
              },
              vAxis: {
                viewWindow:{
#                  max:100,
                  min:0
                }
              },
              width: '100%',
              height: 400
            }
            $('#student-show-game-duration').html('')
            chart = new google.charts.Line($('#student-show-game-duration')[0]);
            chart.draw(charData, options)
    }
  },
  Overview:{
    genderRatio:{
      draw: ->
        $.getJSON '/api/game_result/statistic/gender_ratio', (json)->
          data = google.visualization.arrayToDataTable [
            ['Gender', 'Number of student']
            ['Male', json.M.count],
            ['Fmale', json.F.count]
          ]

          options = {
            title: 'Gender Ratio',
            pieHole: 0.4,
          }

          new google.visualization.PieChart($('#ov-basic-gender')[0]).draw data, options;
    },
    recordFreq:{
      draw: ->
        $.getJSON '/api/game_result/statistic/record_freq', (json)->
          data = [['Date', 'Number of Game Records']]
          sum = 0
          for k, v of json
            sum += parseInt(v)
            data.push [new Date(k), parseInt(v)]
          avg = sum/(data.length-1)

          data = google.visualization.arrayToDataTable data
          options = {
            chart:{
              title: 'Game Record Frequency',
              subtitle: "Average Number of Record: #{Math.round(avg*10)/10}"
            },
            curveType: 'function',
            legend: { position: 'bottom' }
          };

          new google.charts.Line($('#ov-basic-record-freq')[0]).draw data, options;
    },
    durationAvg:{
      data:null,
      drawAll: ->
        draw = (json)->
          Charts.Overview.durationAvg.data = json if Charts.Overview.durationAvg.data is null
          data = [['Date', 'Average Walk Duration (min)']]
          for k, v of json.all.byDate
            data.push [new Date(k), parseInt(v)/60]

          data = google.visualization.arrayToDataTable data
          options = {
            chart:{
              title: 'Average Walk Duration Over Time',
              subtitle: "Average: #{parseInt(json.all.all/60)}min #{Math.round(json.all.all%60)}sec"
            },
            curveType: 'function',
            legend: { position: 'bottom' }
          };

          new google.charts.Line($('#ov-duration-avg')[0]).draw data, options;

        if(Charts.Overview.durationAvg.data == null)
          $.getJSON '/api/game_result/statistic/duration_avg', (json)->
            draw(json)
        else
          draw(Charts.Overview.durationAvg.data)

      drawMale: ->
        draw = (json)->
          Charts.Overview.durationAvg.data = json if Charts.Overview.durationAvg.data is null
          data = [['Date', 'Male Average Walk Duration']]
          for k, v of json.M.byDate
            data.push [new Date(k), parseInt(v)/60]

          data = google.visualization.arrayToDataTable data
          options = {
            chart:{
              title: 'Average Walk Duration Over Time (Male)',
              subtitle: "Average: #{parseInt(json.M.all/60)}min #{Math.round(json.M.all%60)}sec"
            },
            curveType: 'function',
            legend: { position: 'bottom' }
          };

          new google.charts.Line($('#ov-duration-avg-male')[0]).draw data, options;

        if(Charts.Overview.durationAvg.data == null)
          $.getJSON '/api/game_result/statistic/duration_avg', (json)->
            draw(json)
        else
          draw(Charts.Overview.durationAvg.data)


      drawFemale: ->
        draw = (json)->
          Charts.Overview.durationAvg.data = json if Charts.Overview.durationAvg.data is null
          data = [['Date', 'Female Average Walk Duration']]
          for k, v of json.F.byDate
            data.push [new Date(k), parseInt(v)/60]

          data = google.visualization.arrayToDataTable data
          options = {
            chart:{
              title: 'Average Walk Duration Over Time (Female)',
              subtitle: "Average: #{parseInt(json.F.all/60)}min #{Math.round(json.F.all%60)}sec"
            },
            curveType: 'function',
            legend: { position: 'bottom' }
          };

          new google.charts.Line($('#ov-duration-avg-female')[0]).draw data, options;

        if(Charts.Overview.durationAvg.data == null)
          $.getJSON '/api/game_result/statistic/duration_avg', (json)->
            draw(json)
        else
          draw(Charts.Overview.durationAvg.data)
      drawStudentTotal: ->
        $.getJSON '/api/game_result/statistic/duration_student_sum', (json)->
          data = [
            ['Student','Parent', 'Total Walk Time', 'Duration increase/decrease']
            ['Students', null, 0, 0]
          ]
          student_id = [-1]
          studentToolTipData = ['']
          for v, k in json
            data.push([v[2], 'Students', v[1]/60, v[3]/60])
            student_id.push v[0]
            studentToolTipData.push "#{v[1]},#{v[3]}"
          data = google.visualization.arrayToDataTable data

          options = {
            title:'Sum of Walk Duration of each Student (Color: Rate of increase/decrease of last 2 walk)',
            minColor: '#f66',
            midColor: '#ddd',
            maxColor: '#6f3',
            headerHeight: 15,
            fontColor: '#000',
            showScale: true,
            generateTooltip: (id) ->
              return '' if id is 0
              durationSum = studentToolTipData[id].split(',')[0]
              durationDiff = studentToolTipData[id].split(',')[1]
              return $('<div />').addClass('well').append(
                $('<div />').append(
                  $('<span />').css({'font-weight':'bold'}).text('Sum of walk duration: ')
                ).append(
                  $('<span />').css({}).text("#{parseInt(durationSum/60)}m #{parseInt(durationSum%60)}s")
                )
              ).append(
                $('<div />').append(
                  $('<span />').css({'font-weight':'bold'}).text('Duration difference of last 2 walk: ')
                ).append(
                  $('<span />').css({}).text("#{(if durationDiff<0 then '( - )' else '')} #{parseInt(Math.abs(durationDiff)/60)}m #{parseInt(Math.abs(durationDiff)%60)}s")
                )
              ).append(
                $('<div />').css({'border-width':'1px 0 0 0','border-style':'dotted'}).text('Click for more information.')
              )[0].outerHTML

          }

          treemap = new google.visualization.TreeMap($('#ov-duration-student-total')[0])
          google.visualization.events.addListener treemap, 'select', ->
            selectedItem = treemap.getSelection()[0];
            if (selectedItem)
              studId = student_id[selectedItem.row]
              if(studId > 0)
                window.open "/students/#{student_id[selectedItem.row]}"

          treemap.draw data, options

    },
    scenePlaytimes:{
      draw: ->
        $.getJSON '/api/game_result/statistic/scene_playtimes', (json)->
          dataCombine = [['Scene', Scene[1]. name,Scene[2].name]]
          pointer1 = 0
          pointer2 = 0
          while(pointer1 < json.scene1.length && pointer2 < json.scene2.length)
            d1 = new Date(json.scene1[pointer1][0])
            d2 = new Date(json.scene2[pointer2][0])
            if(d1.getTime() < d2.getTime())
              dataCombine.push([d1, json.scene1[pointer1][1], 0])
              pointer1++;
            else if (d2.getTime() > d1.getTime())
              dataCombine.push([d2, 0, json.scene2[pointer2][1]])
              pointer2++;
            else
              dataCombine.push([d1, json.scene1[pointer1][1], json.scene2[pointer2][1]])
              pointer1++;
              pointer2++;

          if(pointer1>= json.scene1.length)
            for v, k in json.scene2
              dataCombine.push [new Date(v[0]), 0, v[1]]

          if(pointer2>= json.scene2.length)
            for v, k in json.scene1
              dataCombine.push [new Date(v[0]), v[1], 0]

          data = google.visualization.arrayToDataTable dataCombine
          options = {
            title:'Scene played on each day',
            legend: { position: 'top', maxLines: 3 },
            bar: { groupWidth: '75%' },
            isStacked: true,
          }
          new google.visualization.ColumnChart($('#ov-scene-playtimes')[0]).draw data, options

    },
    objectCatchAble:{
      draw: ->
        $.getJSON '/api/game_result/statistic/object_catch_able', (json)->
          data = google.visualization.arrayToDataTable [
            ['Ability', 'Number of student']
            ['Can', json.can],
            ['Cannot', json.cannot],
            ['Unknow', json.unknow]
          ]

          options = {
            title: 'Catch Object Ability',
            pieHole: 0.4,
          }

          new google.visualization.PieChart($('#ov-object-able-to-catch')[0]).draw data, options;
    },
    objectCatchAvg:{
      draw: ->
        $.getJSON '/api/game_result/statistic/object_catch_avg', (json)->
          data = [['Date', 'Average Object Caught']]
          for k, v of json.byDate
            data.push [new Date(k), parseInt(v)]

          data = google.visualization.arrayToDataTable data
          options = {
            chart:{
              title: 'Average Object Caught Frequency',
              subtitle: "Overall average: #{Math.round(json.all*10)/10}"
            },
            curveType: 'function',
            legend: { position: 'bottom' }
          };

          new google.charts.Line($('#ov-object-avg-catch')[0]).draw data, options;

    },
  }
}