class ChangeStudentGenderToInteger < ActiveRecord::Migration
  def change
    change_column :students, :gender, "integer USING CASE WHEN gender='M' THEN 0 ELSE 1 END"
  end
end
