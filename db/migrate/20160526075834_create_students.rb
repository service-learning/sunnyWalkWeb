class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.text :name, null:false
      t.text :gender, null:false
      t.text :remarks

      t.timestamps null: false
    end
  end
end
