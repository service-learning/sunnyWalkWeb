class AddSceneIdToGameResult < ActiveRecord::Migration
  def change
    add_column :game_results, :scene_id, :integer, default: 1, null: false
  end
end
