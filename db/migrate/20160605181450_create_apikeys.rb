class CreateApikeys < ActiveRecord::Migration
  def change
    create_table :apikeys do |t|
      t.string :key
      t.string :secret
      t.text :remarks, :null => :true

      t.timestamps null: false
    end
  end
end
