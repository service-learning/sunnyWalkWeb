class CreateGameResults < ActiveRecord::Migration
  def change
    create_table :game_results do |t|
      t.integer :student_id, null:false
      t.foreign_key :students
      t.integer :score, null:true
      t.integer :duration, null:false
      t.integer :object_total, null:false, default: 0
      t.integer :object_caught, null:false, default: 0

      t.timestamps null: false
    end
  end
end
