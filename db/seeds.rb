# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!([
   {
       id: 1,
       email: 'admin@demo.com',
       username: 'admin',
       password: 'adminadmin',
   }
])

Student.create!([
   {
       id:1,
       name:'Cow',
       gender:0,
       remarks:'Cows are passive mobs found in the Overworld.'
   },
   {
       id:2,
       name:'Cave Spider',
       gender:1,
       remarks:'The cave spider is a neutral mob that can inflict poisoning.'
   },
   {
       id:3,
       name:'Zombie Pigman',
       gender:0,
       remarks:'Zombie pigmen are common neutral mobs that live in the Nether.'
   },
   {
       id:4,
       name:'Mooshroom',
       gender:1,
       remarks:'Mooshrooms are unique variations of cows.'
   },
   {
       id:5,
       name:'Slime',
       gender:0,
       remarks:'A Slime is a hostile mob that spawns underground and in swamp biomes.'
   },
   {
       id:6,
       name:'Polar Bear',
       gender:1,
       remarks:'Polar bears are found in ice plains, ice mountains and ice spikes biomes.'
   },
   {
       id:7,
       name:'Silverfish',
       gender:0,
       remarks:'Silverfish have no drops other than 5 experience when killed by a player or tamed wolf.'
   },
   {
       id:8,
       name:'Iron Golem',
       gender:1,
       remarks:'Golems will spawn in a 16×16×6 area, centered between the 21 or more valid doors in a village if it has at least 10 villagers. The chance of spawning is 1 in 7000 per game tick, which averages around one every six minutes. Iron golems can spawn provided the blocks it spawns in are transparent and the block it spawns on top of has a solid surface.'
   },
   {
       id:9,
       name:'Ocelot',
       gender:1,
       remarks:'Ocelots drop 1-3 experience when killed by a player or tamed wolf. Ocelots try to spawn on grass blocks or leaves at sea level or higher in jungle biomes. There is a 1⁄3 chance that a spawn attempt will fail.'
   }
])

GameResult.create!([
   {id: 1, student_id: 3, duration: 859, object_total: 0, object_caught: 0, scene_id: 2, created_at: '18-06-2016T16:17:11Z', updated_at: '18-06-2016T16:17:11Z'},
   {id: 2, student_id: 7, duration: 811, object_total: 73, object_caught: 0, scene_id: 2, created_at: '18-06-2016T08:27:01Z', updated_at: '18-06-2016T08:27:01Z'},
   {id: 3, student_id: 4, duration: 368, object_total: 0, object_caught: 0, scene_id: 1, created_at: '22-06-2016T11:26:53Z', updated_at: '22-06-2016T11:26:53Z'},
   {id: 4, student_id: 4, duration: 516, object_total: 39, object_caught: 0, scene_id: 2, created_at: '18-06-2016T10:42:40Z', updated_at: '18-06-2016T10:42:40Z'},
   {id: 5, student_id: 6, duration: 343, object_total: 0, object_caught: 0, scene_id: 2, created_at: '19-06-2016T14:49:47Z', updated_at: '19-06-2016T14:49:47Z'},
   {id: 6, student_id: 6, duration: 363, object_total: 33, object_caught: 9, scene_id: 2, created_at: '22-06-2016T10:47:17Z', updated_at: '22-06-2016T10:47:17Z'},
   {id: 7, student_id: 7, duration: 836, object_total: 55, object_caught: 6, scene_id: 2, created_at: '20-06-2016T08:41:25Z', updated_at: '20-06-2016T08:41:25Z'},
   {id: 8, student_id: 5, duration: 234, object_total: 16, object_caught: 4, scene_id: 1, created_at: '20-06-2016T12:22:19Z', updated_at: '20-06-2016T12:22:19Z'},
   {id: 9, student_id: 2, duration: 186, object_total: 12, object_caught: 0, scene_id: 2, created_at: '18-06-2016T09:03:45Z', updated_at: '18-06-2016T09:03:45Z'},
   {id: 10, student_id: 5, duration: 540, object_total: 45, object_caught: 0, scene_id: 2, created_at: '22-06-2016T15:58:36Z', updated_at: '22-06-2016T15:58:36Z'},
   {id: 11, student_id: 7, duration: 836, object_total: 76, object_caught: 0, scene_id: 1, created_at: '20-06-2016T13:42:14Z', updated_at: '20-06-2016T13:42:14Z'},
   {id: 12, student_id: 7, duration: 439, object_total: 0, object_caught: 0, scene_id: 3, created_at: '22-06-2016T11:24:35Z', updated_at: '22-06-2016T11:24:35Z'},
   {id: 13, student_id: 4, duration: 971, object_total: 64, object_caught: 0, scene_id: 1, created_at: '21-06-2016T11:11:37Z', updated_at: '21-06-2016T11:11:37Z'},
   {id: 14, student_id: 7, duration: 275, object_total: 18, object_caught: 22, scene_id: 3, created_at: '20-06-2016T15:58:54Z', updated_at: '20-06-2016T15:58:54Z'},
   {id: 15, student_id: 7, duration: 795, object_total: 56, object_caught: 0, scene_id: 2, created_at: '21-06-2016T14:36:06Z', updated_at: '21-06-2016T14:36:06Z'},
   {id: 16, student_id: 3, duration: 591, object_total: 0, object_caught: 0, scene_id: 3, created_at: '22-06-2016T08:06:35Z', updated_at: '22-06-2016T08:06:35Z'},
   {id: 17, student_id: 6, duration: 484, object_total: 0, object_caught: 0, scene_id: 1, created_at: '19-06-2016T11:46:45Z', updated_at: '19-06-2016T11:46:45Z'},
   {id: 18, student_id: 2, duration: 1150, object_total: 0, object_caught: 0, scene_id: 2, created_at: '21-06-2016T09:40:45Z', updated_at: '21-06-2016T09:40:45Z'},
   {id: 19, student_id: 6, duration: 276, object_total: 19, object_caught: 21, scene_id: 1, created_at: '22-06-2016T13:29:17Z', updated_at: '22-06-2016T13:29:17Z'},
   {id: 20, student_id: 7, duration: 554, object_total: 50, object_caught: 13, scene_id: 1, created_at: '18-06-2016T10:18:55Z', updated_at: '18-06-2016T10:18:55Z'},
   {id: 21, student_id: 5, duration: 381, object_total: 27, object_caught: 0, scene_id: 2, created_at: '18-06-2016T13:35:28Z', updated_at: '18-06-2016T13:35:28Z'},
   {id: 22, student_id: 3, duration: 661, object_total: 47, object_caught: 0, scene_id: 1, created_at: '18-06-2016T14:52:39Z', updated_at: '18-06-2016T14:52:39Z'},
   {id: 23, student_id: 1, duration: 781, object_total: 0, object_caught: 0, scene_id: 3, created_at: '19-06-2016T10:08:33Z', updated_at: '19-06-2016T10:08:33Z'},
   {id: 24, student_id: 6, duration: 381, object_total: 38, object_caught: 8, scene_id: 1, created_at: '20-06-2016T12:49:06Z', updated_at: '20-06-2016T12:49:06Z'},
   {id: 25, student_id: 5, duration: 200, object_total: 14, object_caught: 0, scene_id: 3, created_at: '22-06-2016T14:42:43Z', updated_at: '22-06-2016T14:42:43Z'},
   {id: 26, student_id: 5, duration: 333, object_total: 25, object_caught: 0, scene_id: 1, created_at: '20-06-2016T09:54:09Z', updated_at: '20-06-2016T09:54:09Z'},
   {id: 27, student_id: 7, duration: 1040, object_total: 0, object_caught: 0, scene_id: 3, created_at: '20-06-2016T11:17:31Z', updated_at: '20-06-2016T11:17:31Z'},
   {id: 28, student_id: 2, duration: 706, object_total: 58, object_caught: 5, scene_id: 1, created_at: '19-06-2016T11:24:00Z', updated_at: '19-06-2016T11:24:00Z'},
   {id: 29, student_id: 3, duration: 833, object_total: 69, object_caught: 0, scene_id: 1, created_at: '20-06-2016T10:25:41Z', updated_at: '20-06-2016T10:25:41Z'},
   {id: 30, student_id: 3, duration: 1042, object_total: 0, object_caught: 0, scene_id: 3, created_at: '21-06-2016T14:43:35Z', updated_at: '21-06-2016T14:43:35Z'},
   {id: 31, student_id: 5, duration: 502, object_total: 50, object_caught: 0, scene_id: 3, created_at: '18-06-2016T10:47:25Z', updated_at: '18-06-2016T10:47:25Z'},
   {id: 32, student_id: 7, duration: 299, object_total: 0, object_caught: 0, scene_id: 1, created_at: '22-06-2016T14:51:22Z', updated_at: '22-06-2016T14:51:22Z'},
   {id: 33, student_id: 5, duration: 764, object_total: 69, object_caught: 13, scene_id: 1, created_at: '22-06-2016T10:27:07Z', updated_at: '22-06-2016T10:27:07Z'},
   {id: 34, student_id: 6, duration: 250, object_total: 17, object_caught: 0, scene_id: 1, created_at: '19-06-2016T09:30:40Z', updated_at: '19-06-2016T09:30:40Z'},
   {id: 35, student_id: 4, duration: 610, object_total: 0, object_caught: 0, scene_id: 3, created_at: '22-06-2016T14:12:20Z', updated_at: '22-06-2016T14:12:20Z'},
   {id: 36, student_id: 7, duration: 639, object_total: 45, object_caught: 61, scene_id: 3, created_at: '20-06-2016T14:33:04Z', updated_at: '20-06-2016T14:33:04Z'},
   {id: 37, student_id: 4, duration: 1128, object_total: 112, object_caught: 0, scene_id: 2, created_at: '22-06-2016T10:43:32Z', updated_at: '22-06-2016T10:43:32Z'},
   {id: 38, student_id: 7, duration: 752, object_total: 57, object_caught: 0, scene_id: 1, created_at: '21-06-2016T16:28:08Z', updated_at: '21-06-2016T16:28:08Z'},
   {id: 39, student_id: 5, duration: 778, object_total: 0, object_caught: 0, scene_id: 1, created_at: '20-06-2016T08:05:34Z', updated_at: '20-06-2016T08:05:34Z'},
   {id: 40, student_id: 5, duration: 376, object_total: 34, object_caught: 0, scene_id: 1, created_at: '21-06-2016T12:07:03Z', updated_at: '21-06-2016T12:07:03Z'},
   {id: 41, student_id: 1, duration: 1086, object_total: 83, object_caught: 42, scene_id: 3, created_at: '19-06-2016T16:31:35Z', updated_at: '19-06-2016T16:31:35Z'},
   {id: 42, student_id: 3, duration: 598, object_total: 49, object_caught: 0, scene_id: 3, created_at: '19-06-2016T10:17:54Z', updated_at: '19-06-2016T10:17:54Z'},
   {id: 43, student_id: 2, duration: 246, object_total: 0, object_caught: 0, scene_id: 3, created_at: '20-06-2016T12:33:16Z', updated_at: '20-06-2016T12:33:16Z'},
   {id: 44, student_id: 3, duration: 1067, object_total: 88, object_caught: 0, scene_id: 2, created_at: '18-06-2016T14:14:12Z', updated_at: '18-06-2016T14:14:12Z'},
   {id: 45, student_id: 4, duration: 555, object_total: 50, object_caught: 0, scene_id: 2, created_at: '20-06-2016T13:52:45Z', updated_at: '20-06-2016T13:52:45Z'},
   {id: 46, student_id: 1, duration: 736, object_total: 49, object_caught: 20, scene_id: 1, created_at: '18-06-2016T11:27:27Z', updated_at: '18-06-2016T11:27:27Z'},
   {id: 47, student_id: 1, duration: 810, object_total: 73, object_caught: 0, scene_id: 2, created_at: '19-06-2016T08:24:00Z', updated_at: '19-06-2016T08:24:00Z'},
   {id: 48, student_id: 3, duration: 998, object_total: 0, object_caught: 0, scene_id: 3, created_at: '21-06-2016T09:04:54Z', updated_at: '21-06-2016T09:04:54Z'},
   {id: 49, student_id: 6, duration: 678, object_total: 61, object_caught: 16, scene_id: 3, created_at: '22-06-2016T09:25:03Z', updated_at: '22-06-2016T09:25:03Z'},
   {id: 50, student_id: 7, duration: 462, object_total: 33, object_caught: 8, scene_id: 3, created_at: '19-06-2016T15:05:37Z', updated_at: '19-06-2016T15:05:37Z'},
   {id: 51, student_id: 5, duration: 1167, object_total: 89, object_caught: 0, scene_id: 3, created_at: '22-06-2016T11:28:02Z', updated_at: '22-06-2016T11:28:02Z'},
   {id: 52, student_id: 7, duration: 489, object_total: 0, object_caught: 0, scene_id: 2, created_at: '19-06-2016T12:44:30Z', updated_at: '19-06-2016T12:44:30Z'},
   {id: 53, student_id: 2, duration: 1081, object_total: 83, object_caught: 0, scene_id: 1, created_at: '19-06-2016T15:17:34Z', updated_at: '19-06-2016T15:17:34Z'},
   {id: 54, student_id: 1, duration: 750, object_total: 75, object_caught: 0, scene_id: 3, created_at: '22-06-2016T15:51:24Z', updated_at: '22-06-2016T15:51:24Z'},
   {id: 55, student_id: 2, duration: 816, object_total: 0, object_caught: 0, scene_id: 2, created_at: '18-06-2016T12:14:33Z', updated_at: '18-06-2016T12:14:33Z'},
   {id: 56, student_id: 1, duration: 1112, object_total: 101, object_caught: 55, scene_id: 3, created_at: '22-06-2016T10:50:01Z', updated_at: '22-06-2016T10:50:01Z'},
   {id: 57, student_id: 3, duration: 1169, object_total: 97, object_caught: 0, scene_id: 2, created_at: '18-06-2016T14:37:58Z', updated_at: '18-06-2016T14:37:58Z'},
   {id: 58, student_id: 4, duration: 1062, object_total: 0, object_caught: 0, scene_id: 3, created_at: '18-06-2016T09:50:41Z', updated_at: '18-06-2016T09:50:41Z'},
   {id: 59, student_id: 7, duration: 908, object_total: 90, object_caught: 0, scene_id: 1, created_at: '22-06-2016T10:42:23Z', updated_at: '22-06-2016T10:42:23Z'},
   {id: 60, student_id: 3, duration: 904, object_total: 64, object_caught: 0, scene_id: 2, created_at: '20-06-2016T09:29:14Z', updated_at: '20-06-2016T09:29:14Z'},
   {id: 61, student_id: 4, duration: 974, object_total: 0, object_caught: 0, scene_id: 3, created_at: '21-06-2016T13:37:55Z', updated_at: '21-06-2016T13:37:55Z'},
   {id: 62, student_id: 6, duration: 214, object_total: 0, object_caught: 0, scene_id: 1, created_at: '18-06-2016T12:02:36Z', updated_at: '18-06-2016T12:02:36Z'},
   {id: 63, student_id: 1, duration: 1099, object_total: 0, object_caught: 0, scene_id: 3, created_at: '19-06-2016T16:07:58Z', updated_at: '19-06-2016T16:07:58Z'},
   {id: 64, student_id: 6, duration: 985, object_total: 70, object_caught: 0, scene_id: 1, created_at: '21-06-2016T11:53:05Z', updated_at: '21-06-2016T11:53:05Z'},
   {id: 65, student_id: 5, duration: 248, object_total: 0, object_caught: 0, scene_id: 1, created_at: '18-06-2016T15:31:15Z', updated_at: '18-06-2016T15:31:15Z'},
   {id: 66, student_id: 4, duration: 234, object_total: 15, object_caught: 16, scene_id: 1, created_at: '18-06-2016T13:54:37Z', updated_at: '18-06-2016T13:54:37Z'},
   {id: 67, student_id: 1, duration: 908, object_total: 69, object_caught: 18, scene_id: 3, created_at: '22-06-2016T16:07:32Z', updated_at: '22-06-2016T16:07:32Z'},
   {id: 68, student_id: 2, duration: 708, object_total: 64, object_caught: 0, scene_id: 3, created_at: '18-06-2016T14:31:55Z', updated_at: '18-06-2016T14:31:55Z'},
   {id: 69, student_id: 6, duration: 791, object_total: 0, object_caught: 0, scene_id: 2, created_at: '20-06-2016T08:53:57Z', updated_at: '20-06-2016T08:53:57Z'},
   {id: 70, student_id: 4, duration: 621, object_total: 0, object_caught: 0, scene_id: 1, created_at: '22-06-2016T09:45:22Z', updated_at: '22-06-2016T09:45:22Z'},
   {id: 71, student_id: 4, duration: 377, object_total: 34, object_caught: 12, scene_id: 2, created_at: '20-06-2016T15:00:00Z', updated_at: '20-06-2016T15:00:00Z'},
   {id: 72, student_id: 2, duration: 180, object_total: 15, object_caught: 26, scene_id: 2, created_at: '22-06-2016T09:06:20Z', updated_at: '22-06-2016T09:06:20Z'},
   {id: 73, student_id: 7, duration: 583, object_total: 58, object_caught: 0, scene_id: 2, created_at: '18-06-2016T13:06:40Z', updated_at: '18-06-2016T13:06:40Z'},
   {id: 74, student_id: 4, duration: 808, object_total: 0, object_caught: 0, scene_id: 3, created_at: '22-06-2016T10:59:05Z', updated_at: '22-06-2016T10:59:05Z'},
   {id: 75, student_id: 1, duration: 835, object_total: 0, object_caught: 0, scene_id: 3, created_at: '20-06-2016T14:45:27Z', updated_at: '20-06-2016T14:45:27Z'},
   {id: 76, student_id: 5, duration: 222, object_total: 0, object_caught: 0, scene_id: 2, created_at: '21-06-2016T09:01:35Z', updated_at: '21-06-2016T09:01:35Z'},
   {id: 77, student_id: 5, duration: 1158, object_total: 0, object_caught: 0, scene_id: 2, created_at: '22-06-2016T15:31:24Z', updated_at: '22-06-2016T15:31:24Z'},
   {id: 78, student_id: 4, duration: 936, object_total: 0, object_caught: 0, scene_id: 3, created_at: '20-06-2016T11:05:08Z', updated_at: '20-06-2016T11:05:08Z'},
   {id: 79, student_id: 1, duration: 1078, object_total: 77, object_caught: 0, scene_id: 1, created_at: '19-06-2016T09:57:53Z', updated_at: '19-06-2016T09:57:53Z'},
   {id: 80, student_id: 6, duration: 448, object_total: 0, object_caught: 0, scene_id: 3, created_at: '20-06-2016T13:47:51Z', updated_at: '20-06-2016T13:47:51Z'},
   {id: 81, student_id: 5, duration: 295, object_total: 22, object_caught: 0, scene_id: 3, created_at: '19-06-2016T14:44:18Z', updated_at: '19-06-2016T14:44:18Z'},
   {id: 82, student_id: 7, duration: 805, object_total: 0, object_caught: 0, scene_id: 2, created_at: '20-06-2016T12:35:43Z', updated_at: '20-06-2016T12:35:43Z'},
   {id: 83, student_id: 5, duration: 258, object_total: 17, object_caught: 0, scene_id: 1, created_at: '22-06-2016T12:16:25Z', updated_at: '22-06-2016T12:16:25Z'},
   {id: 84, student_id: 4, duration: 384, object_total: 29, object_caught: 0, scene_id: 2, created_at: '22-06-2016T10:43:49Z', updated_at: '22-06-2016T10:43:49Z'},
   {id: 85, student_id: 3, duration: 645, object_total: 0, object_caught: 0, scene_id: 2, created_at: '22-06-2016T10:00:37Z', updated_at: '22-06-2016T10:00:37Z'},
   {id: 86, student_id: 4, duration: 701, object_total: 0, object_caught: 0, scene_id: 2, created_at: '22-06-2016T09:37:26Z', updated_at: '22-06-2016T09:37:26Z'},
   {id: 87, student_id: 7, duration: 884, object_total: 0, object_caught: 0, scene_id: 3, created_at: '22-06-2016T10:38:04Z', updated_at: '22-06-2016T10:38:04Z'},
   {id: 88, student_id: 3, duration: 583, object_total: 0, object_caught: 0, scene_id: 2, created_at: '22-06-2016T14:47:20Z', updated_at: '22-06-2016T14:47:20Z'},
   {id: 89, student_id: 1, duration: 361, object_total: 32, object_caught: 0, scene_id: 1, created_at: '21-06-2016T11:26:36Z', updated_at: '21-06-2016T11:26:36Z'},
   {id: 90, student_id: 4, duration: 571, object_total: 0, object_caught: 0, scene_id: 2, created_at: '21-06-2016T12:53:43Z', updated_at: '21-06-2016T12:53:43Z'},
   {id: 91, student_id: 6, duration: 421, object_total: 30, object_caught: 0, scene_id: 3, created_at: '20-06-2016T10:40:48Z', updated_at: '20-06-2016T10:40:48Z'},
   {id: 92, student_id: 5, duration: 1099, object_total: 91, object_caught: 2, scene_id: 1, created_at: '18-06-2016T13:00:03Z', updated_at: '18-06-2016T13:00:03Z'},
   {id: 93, student_id: 4, duration: 432, object_total: 33, object_caught: 9, scene_id: 3, created_at: '21-06-2016T16:36:46Z', updated_at: '21-06-2016T16:36:46Z'},
   {id: 94, student_id: 2, duration: 277, object_total: 23, object_caught: 0, scene_id: 3, created_at: '20-06-2016T10:36:29Z', updated_at: '20-06-2016T10:36:29Z'},
   {id: 95, student_id: 7, duration: 564, object_total: 0, object_caught: 0, scene_id: 2, created_at: '20-06-2016T09:18:43Z', updated_at: '20-06-2016T09:18:43Z'},
   {id: 96, student_id: 3, duration: 784, object_total: 56, object_caught: 29, scene_id: 2, created_at: '21-06-2016T10:29:08Z', updated_at: '21-06-2016T10:29:08Z'},
   {id: 97, student_id: 4, duration: 1164, object_total: 83, object_caught: 16, scene_id: 3, created_at: '21-06-2016T13:00:37Z', updated_at: '21-06-2016T13:00:37Z'},
   {id: 98, student_id: 3, duration: 344, object_total: 24, object_caught: 0, scene_id: 1, created_at: '21-06-2016T09:28:05Z', updated_at: '21-06-2016T09:28:05Z'},
   {id: 99, student_id: 2, duration: 1176, object_total: 0, object_caught: 0, scene_id: 1, created_at: '18-06-2016T10:51:45Z', updated_at: '18-06-2016T10:51:45Z'},
   {id: 100, student_id: 5, duration: 335, object_total: 27, object_caught: 0, scene_id: 1, created_at: '19-06-2016T15:38:53Z', updated_at: '19-06-2016T15:38:53Z'},
])