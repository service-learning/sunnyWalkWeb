# SunnyWalkWeb

**Source code download: [v1.1](https://gitlab.com/service-learning/sunnyWalkWeb/repository/archive.zip?ref=v1.1) or [latest version](https://gitlab.com/service-learning/sunnyWalkWeb/repository/archive.zip?ref=dev)**

## Online version
* The application deployed on Heroku: [https://sunnywalk.herokuapp.com/](https://sunnywalk.herokuapp.com/)
* Default username: `admin`, password: `adminadmin`
* Please change the password: 
[https://sunnywalk.herokuapp.com/users/edit](https://sunnywalk.herokuapp.com/users/edit)

## Custom server hosting on a Ubuntu Linux 14.04 Server
 Install `Ruby 2.3.0` and `Rails 4.2.6` 


```
#!bash

# Install Ruby 2.3.0
sudo apt-get update
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev
sudo apt-get install libgdbm-dev libncurses5-dev automake libtool bison libffi-dev
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sSL https://get.rvm.io | bash -s stable
source ~/.rvm/scripts/rvm
rvm install 2.3.0
rvm use 2.3.0 --default
gem install bundler

# Install Rails 4.2.6
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs
gem install rails -v 4.2.6
```


## Install and setup PostgreSQL 9.3

```
#!bash

sudo apt-get install postgresql libpq-dev
sudo -i -u postgres
createuser -s YOUR_USERNAME
createdb sunny_walk_development
createdb sunny_walk_production
```

## Run the application

```
#!bash

cd sunnyWalkWeb
rake db:setup
rails s
```
Access web server at `http://127.0.0.1:3000`

## API
* For more information on the API, please go to our [Wiki](https://bitbucket.org/ronald8192/sunnywalkweb/wiki/Home)