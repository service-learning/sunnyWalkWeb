# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.scss, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

[
    %w(students_index.js game_results_index.js student_show.js admin/apikey_index.js overview.js SceneMapping.js)
].each do |js|
  Rails.application.config.assets.precompile += js
end